﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using Newtonsoft.Json;
using NSubstitute;
using NUnit.Framework;
using Recruitment.API.Configuration;
using Recruitment.API.Exceptions;
using Recruitment.API.Queries.GetLoginPasswordHash;
using Recruitment.Contracts;

namespace Recruitment.Tests.Queries
{
    [TestFixture]
    public class GetLoginPasswordHashQueryTest
    {
        [Test]
        public async Task GetLoginPasswordHashQuery_ReturnsCorrectResult()
        {
            // given
            var messageHandler = new HttpMessageHandlerMock(JsonConvert.SerializeObject(new HashResponseDto{Hash_value = "hash-value"}), HttpStatusCode.OK);
            var httpClientFactory = Substitute.For<IHttpClientFactory>();
            httpClientFactory.CreateClient().Returns(new HttpClient(messageHandler));
            var queryHandler = new GetLoginPasswordHashQueryHandler(httpClientFactory, new ApplicationSettingsConfiguration{AzureFunctionsBaseUrl = "http://test"});

            // when
            var result = await queryHandler.Handle(new GetLoginPasswordHashQuery(){Login = "test", Password = "password"}, CancellationToken.None);
            
            // then
            result.Should().NotBeNull();
            result.Hash_value.Should().BeEquivalentTo("hash-value");
        }
        
        [Test]
        public async Task GetLoginPasswordHashQuery_ReturnsErrorMessage_ForInternalServerError()
        {
            // given
            var messageHandler = new HttpMessageHandlerMock(JsonConvert.SerializeObject(new ErrorResponseDto{Message = "test error"}), HttpStatusCode.InternalServerError);
            var httpClientFactory = Substitute.For<IHttpClientFactory>();
            httpClientFactory.CreateClient().Returns(new HttpClient(messageHandler));
            var queryHandler = new GetLoginPasswordHashQueryHandler(httpClientFactory, new ApplicationSettingsConfiguration{AzureFunctionsBaseUrl = "http://test"});

            // when
            try
            {
                await queryHandler.Handle(new GetLoginPasswordHashQuery(), CancellationToken.None);
                throw new AssertionException("Should not happen!");
            }
            catch (AzureFunctionCallException e)
            {
                // then
                e.StatusCode.Should().BeEquivalentTo(HttpStatusCode.InternalServerError);
                e.Message.Should().BeEquivalentTo("test error");
            }
        }
        
        [Test]
        public async Task GetLoginPasswordHashQuery_ReturnsErrorMessage_ForOtherStatusCode()
        {
            // given
            var messageHandler = new HttpMessageHandlerMock("bad gateway", HttpStatusCode.BadGateway);
            var httpClientFactory = Substitute.For<IHttpClientFactory>();
            httpClientFactory.CreateClient().Returns(new HttpClient(messageHandler));
            var queryHandler = new GetLoginPasswordHashQueryHandler(httpClientFactory, new ApplicationSettingsConfiguration{AzureFunctionsBaseUrl = "http://test"});

            // when
            try
            {
                await queryHandler.Handle(new GetLoginPasswordHashQuery(), CancellationToken.None);
                throw new AssertionException("Should not happen!");
            }
            catch (AzureFunctionCallException e)
            {
                // then
                e.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadGateway);
                e.Message.Should().BeEquivalentTo($"bad gateway ({HttpStatusCode.BadGateway} Code={(int)HttpStatusCode.BadGateway})");
            }
        }
        
        [Test]
        public async Task GetLoginPasswordHashQuery_ReturnsErrorMessage_ForBadMessage()
        {
            // given
            var messageHandler = new HttpMessageHandlerMock("bad message", HttpStatusCode.InternalServerError);
            var httpClientFactory = Substitute.For<IHttpClientFactory>();
            httpClientFactory.CreateClient().Returns(new HttpClient(messageHandler));
            var queryHandler = new GetLoginPasswordHashQueryHandler(httpClientFactory, new ApplicationSettingsConfiguration{AzureFunctionsBaseUrl = "http://test"});

            // when
            try
            {
                await queryHandler.Handle(new GetLoginPasswordHashQuery(), CancellationToken.None);
                throw new AssertionException("Should not happen!");
            }
            catch (Exception e)
            {
                // then
                e.Should().NotBeNull();
            }
        }
    }
}