﻿using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NUnit.Framework;
using Recruitment.Contracts;
using Recruitment.Functions;
using Recruitment.Tests.Functions.Helpers;

namespace Recruitment.Tests.Functions
{
    [TestFixture]
    public class HashingFunctionTest
    {
        private readonly ILogger _logger = FunctionsTestHelper.CreateLogger();
        
        [Test]
        public async Task HashingFunction_IsReturningHash()
        {
            // given
            var httpRequest = FunctionsTestHelper.CreateHttpRequest(JsonConvert.SerializeObject(new HashRequestDto {Login = "Example login", Password = "Example password"}));
            
            // when
            var responseMessage = await HttpTriggerFunctions.RunAsync(httpRequest, _logger);
            var responseString = await responseMessage.Content.ReadAsStringAsync();
            var response = JsonConvert.DeserializeObject<HashResponseDto>(responseString);
            
            // then
            response.Should().NotBeNull();
            response.Hash_value.Should().BeEquivalentTo("ca397befc46016f628a450aff91a0184");
        }
        
        [Test]
        public async Task HashingFunction_IsReturning404_ForBadBody()
        {
            // given
            var httpRequest = FunctionsTestHelper.CreateHttpRequest("bad body request");
            
            // when
            var responseMessage = await HttpTriggerFunctions.RunAsync(httpRequest, _logger);
            var responseString = await responseMessage.Content.ReadAsStringAsync();
            var response = JsonConvert.DeserializeObject<ErrorResponseDto>(responseString);
            
            // then
            response.Should().NotBeNull();
            response.Message.Should().StartWithEquivalent("Could not parse request body.");
        }
        
        [Test]
        public async Task HashingFunction_IsReturning404_ForEmptyData()
        {
            // given
            var httpRequest = FunctionsTestHelper.CreateHttpRequest(JsonConvert.SerializeObject(new HashRequestDto()));
            
            // when
            var responseMessage = await HttpTriggerFunctions.RunAsync(httpRequest, _logger);
            var responseString = await responseMessage.Content.ReadAsStringAsync();
            var response = JsonConvert.DeserializeObject<ErrorResponseDto>(responseString);
            
            // then
            response.Should().NotBeNull();
            response.Message.Should().StartWithEquivalent("Login and/or password cannot be null or empty!");
        }
    }
}