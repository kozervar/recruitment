﻿using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace Recruitment.Tests.Functions.Helpers
{
    public class FunctionsTestHelper
    {
        public static HttpRequest CreateHttpRequest(string body)
        {
            var context = new DefaultHttpContext();
            var request = context.Request;
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(body);
            writer.Flush();
            stream.Position = 0;
            request.Body = stream;
            return request;
        }

        public static ILogger CreateLogger(FunctionsLoggerTypes type = FunctionsLoggerTypes.Null)
        {
            ILogger logger;

            if (type == FunctionsLoggerTypes.List)
            {
                logger = new FunctionsListLogger();
            }
            else
            {
                logger = NullLoggerFactory.Instance.CreateLogger("Null Logger");
            }

            return logger;
        }
    }
}