﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace Recruitment.Tests.Functions.Helpers
{
    public class FunctionsListLogger : ILogger
    {
        public IList<string> Logs;

        public IDisposable BeginScope<TState>(TState state) => FunctionsNullScope.Instance;

        public bool IsEnabled(LogLevel logLevel) => false;

        public FunctionsListLogger()
        {
            Logs = new List<string>();
        }

        public void Log<TState>(LogLevel logLevel,
            EventId eventId,
            TState state,
            Exception exception,
            Func<TState, Exception, string> formatter)
        {
            string message = formatter(state, exception);
            Logs.Add(message);
        }
    }
}