﻿using System;

namespace Recruitment.Tests.Functions.Helpers
{
    public class FunctionsNullScope : IDisposable
    {
        public static FunctionsNullScope Instance { get; } = new FunctionsNullScope();

        private FunctionsNullScope() { }

        public void Dispose() { }
    }
}