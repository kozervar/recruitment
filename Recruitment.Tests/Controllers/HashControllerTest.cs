﻿using System;
using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;
using Recruitment.API.Controllers;
using Recruitment.API.Exceptions;
using Recruitment.API.Queries.GetLoginPasswordHash;
using Recruitment.Contracts;

namespace Recruitment.Tests.Controllers
{
    [TestFixture]
    public class HashControllerTest
    {
        [Test]
        public async Task GetLoginPasswordHash_ShouldExecute()
        {
            // given
            var logger = Substitute.For<ILogger<HashController>>();
            var mediator = Substitute.For<IMediator>();
            mediator.Send(Arg.Any<GetLoginPasswordHashQuery>()).Returns(new HashResponseDto {Hash_value = "test"});
            var controller = new HashController(logger, mediator);
            
            // when
            var response = await controller.GetLoginPasswordHash(new GetLoginPasswordHashQuery() {Login = "test", Password = "test"}) as OkObjectResult;
            
            // then
            response.Should().NotBeNull();
            ((HashResponseDto)response.Value).Hash_value.Should().BeEquivalentTo("test");
        }
        
        [Test]
        public async Task GetLoginPasswordHash_ShouldReturnError_ForAzureFunctionException()
        {
            // given
            var logger = Substitute.For<ILogger<HashController>>();
            var mediator = Substitute.For<IMediator>();
            mediator.Send(Arg.Any<GetLoginPasswordHashQuery>()).Throws(new AzureFunctionCallException("error", HttpStatusCode.BadRequest));
            var controller = new HashController(logger, mediator);
            
            // when
            var response = await controller.GetLoginPasswordHash(new GetLoginPasswordHashQuery() {Login = "test", Password = "test"}) as ObjectResult;
            
            // then
            response.Should().NotBeNull();
            response.StatusCode.Should().Be(400);
            ((ErrorResponseDto)response.Value).Message.Should().BeEquivalentTo("error");
        }
        
        [Test]
        public async Task GetLoginPasswordHash_ShouldReturnError_ForOtherException()
        {
            // given
            var logger = Substitute.For<ILogger<HashController>>();
            var mediator = Substitute.For<IMediator>();
            mediator.Send(Arg.Any<GetLoginPasswordHashQuery>()).Throws(new Exception("error"));
            var controller = new HashController(logger, mediator);
            
            // when
            var response = await controller.GetLoginPasswordHash(new GetLoginPasswordHashQuery() {Login = "test", Password = "test"}) as ObjectResult;
            
            // then
            response.Should().NotBeNull();
            response.StatusCode.Should().Be(500);
            ((ErrorResponseDto)response.Value).Message.Should().BeEquivalentTo("error");
        }
    }
}