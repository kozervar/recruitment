﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Recruitment.Contracts;

namespace Recruitment.Functions
{
    public static class HttpTriggerFunctions
    {
        [FunctionName(FunctionNames.HashingFunction)]
        public static async Task<HttpResponseMessage> RunAsync([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]
            HttpRequest req, ILogger log)
        {
            log.LogInformation($"Executing: {FunctionNames.HashingFunction}");
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            HashRequestDto requestDto = null;
            
            try
            {
                requestDto = JsonConvert.DeserializeObject<HashRequestDto>(requestBody);
                if (string.IsNullOrEmpty(requestDto.Login) || string.IsNullOrEmpty(requestDto.Password))
                {
                    log.LogWarning($"Function failed: {FunctionNames.HashingFunction}");
                    return GetJsonHttpResponseMessage(HttpStatusCode.BadRequest, new ErrorResponseDto {Message = $"Login and/or password cannot be null or empty!"});
                }
            }
            catch (Exception exception)
            {
                log.LogError(exception, "Could not parse request body");
                log.LogWarning($"Function failed: {FunctionNames.HashingFunction}");
                return GetJsonHttpResponseMessage(HttpStatusCode.BadRequest, new ErrorResponseDto {Message = $"Could not parse request body. {exception.Message}" });
            }
            
            try
            {
                var bytes = new UTF8Encoding().GetBytes($"{requestDto.Login}{requestDto.Password}");
                using (var md5 = System.Security.Cryptography.MD5.Create())
                {
                    var hashBytes = md5.ComputeHash(bytes);
                    var hash = BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
                    log.LogInformation($"Function executed: {FunctionNames.HashingFunction}");
                    return GetJsonHttpResponseMessage(HttpStatusCode.OK, new HashResponseDto {Hash_value = hash});
                }
            }
            catch (Exception exception)
            {
                log.LogError(exception, "Could not calculate hash");
                log.LogWarning($"Function failed: {FunctionNames.HashingFunction}");
                return GetJsonHttpResponseMessage(HttpStatusCode.InternalServerError, new ErrorResponseDto {Message = $"Could not calculate hash. {exception.Message}" });
            }
        }

        private static HttpResponseMessage GetJsonHttpResponseMessage(HttpStatusCode statusCode, object content)
        {
            return new HttpResponseMessage(statusCode) {
                Content = new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, MediaTypeNames.Application.Json)
            };
        }
    }
}