﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Recruitment.Contracts
{
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class HashRequestDto
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}