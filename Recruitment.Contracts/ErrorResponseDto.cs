﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Recruitment.Contracts
{
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class ErrorResponseDto
    {
        public string Message { get; set; }
    }
}