﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Recruitment.Contracts
{
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class HashResponseDto
    {
        public string Hash_value { get; set; }
    }
}