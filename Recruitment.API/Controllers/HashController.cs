﻿using System;
using System.Net;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Recruitment.API.Exceptions;
using Recruitment.API.Queries.GetLoginPasswordHash;
using Recruitment.Contracts;
using Swashbuckle.AspNetCore.Annotations;

namespace Recruitment.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HashController : Controller
    {
        private readonly ILogger<HashController> _logger;
        private readonly IMediator _mediator;

        public HashController(ILogger<HashController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpPost]
        [SwaggerOperation(
            Summary = "Returns MD5 hash for provided login and password",
            OperationId = "GetLoginPasswordHash",
            Tags = new[] { "Hash" }
        )]
        public async Task<IActionResult> GetLoginPasswordHash([FromBody] GetLoginPasswordHashQuery request)
        {
            try
            {
                var response = await _mediator.Send(request);
                return Ok(response);
            }
            catch (AzureFunctionCallException e)
            {
                _logger.LogError(e, "Azure function call exception");
                return StatusCode((int) e.StatusCode, new ErrorResponseDto{Message = e.Message});
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Unexpected exception");
                return StatusCode((int) HttpStatusCode.InternalServerError, new ErrorResponseDto{Message = e.Message});
            }
        }
    }
}