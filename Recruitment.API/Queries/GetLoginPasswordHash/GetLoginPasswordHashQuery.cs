﻿using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Newtonsoft.Json;
using Recruitment.API.Configuration;
using Recruitment.API.Exceptions;
using Recruitment.Contracts;

namespace Recruitment.API.Queries.GetLoginPasswordHash
{
    public class GetLoginPasswordHashQuery : HashRequestDto, IRequest<HashResponseDto>
    {
    }

    public class GetLoginPasswordHashQueryHandler : IRequestHandler<GetLoginPasswordHashQuery, HashResponseDto>
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ApplicationSettingsConfiguration _applicationSettingsConfiguration;

        public GetLoginPasswordHashQueryHandler(IHttpClientFactory httpClientFactory, ApplicationSettingsConfiguration applicationSettingsConfiguration)
        {
            _httpClientFactory = httpClientFactory;
            _applicationSettingsConfiguration = applicationSettingsConfiguration;
        }

        public async Task<HashResponseDto> Handle(GetLoginPasswordHashQuery request, CancellationToken cancellationToken)
        {
            using (HttpClient client = _httpClientFactory.CreateClient()) 
            { 
                var stringContent = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, MediaTypeNames.Application.Json);

                using (HttpResponseMessage res = await client.PostAsync($"{_applicationSettingsConfiguration.AzureFunctionsBaseUrl}/{FunctionNames.HashingFunction}", stringContent, cancellationToken)) 
                {
                    using (HttpContent content = res.Content) 
                    { 
                        var data = await content.ReadAsStringAsync();
                        if (res.IsSuccessStatusCode)
                        {
                            return JsonConvert.DeserializeObject<HashResponseDto>(data);
                        }
                        if(res.StatusCode == HttpStatusCode.BadRequest || res.StatusCode == HttpStatusCode.InternalServerError)
                        {
                            var errorResponseDto = JsonConvert.DeserializeObject<ErrorResponseDto>(data);
                            throw new AzureFunctionCallException($"{errorResponseDto?.Message}".Trim(), res.StatusCode);
                        }
                        throw new AzureFunctionCallException($"{data} ({res.StatusCode} Code={(int) res.StatusCode})".Trim(), res.StatusCode);
                    }
                } 
            } 
        }
    }
}