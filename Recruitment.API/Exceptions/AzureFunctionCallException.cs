﻿using System;
using System.Net;

namespace Recruitment.API.Exceptions
{
    public class AzureFunctionCallException : Exception
    {
        public HttpStatusCode StatusCode { get; }

        public AzureFunctionCallException(string message) : base(message)
        {
        }
        public AzureFunctionCallException(string message, HttpStatusCode statusCode) : base(message)
        {
            StatusCode = statusCode;
        }
    }
}