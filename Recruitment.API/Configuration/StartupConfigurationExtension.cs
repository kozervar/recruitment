﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Recruitment.API.Configuration
{
    public static class StartupConfigurationExtension
    {
        public static IServiceCollection ConfigurationBinding(this IServiceCollection services, IConfiguration configuration)
        {            
            var applicationSettingsConfiguration = new ApplicationSettingsConfiguration();
            configuration.Bind("ApplicationSettings", applicationSettingsConfiguration);
            
            return services.AddSingleton(applicationSettingsConfiguration);
        }
    }
}