﻿namespace Recruitment.API.Configuration
{
    public class ApplicationSettingsConfiguration
    {
        public string AzureFunctionsBaseUrl { get; set; }
    }
}